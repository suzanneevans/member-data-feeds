-- unit tests in audit mode (mostly manual)

---- test age changes
--SELECT	md.member_reference, md.age as OldAgeValue,  m.Member_Age as NewAgeValue
--FROM [KS_DEV].[dbo].member_details md
--join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m on md.Member_Reference collate Latin1_General_CI_AI = m.plan_member_ref
----where md.Date_Last_Updated >= '28-Jun-2017'
--where m.plan_member_id in ('19892490','19893567','19893661','19897771','19902892','19902999','19906795','19908478','19911834','19892594','19894612','19894740','19895003','19895033','19900532','19903055','19907635','19911962','19914233','19914776','19916387','19916693','19905781','19893981','19914054','19914448','19902816','19915014','19895034')
----and md.age = m.Member_Age

---- email
--SELECT	md.member_reference, md.Email_Address as OldEmailValue,  m.email_address as NewEmailValue
--FROM [KS_DEV].[dbo].member_details md
--join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m on md.Member_Reference collate Latin1_General_CI_AI = m.plan_member_ref
----where md.Date_Last_Updated >= '28-Jun-2017'
--where m.plan_member_id in ('19892882','19893013','19892484','19892495','19892527','19893543','19895830','19893391','19893599','19896348','19894302','19897724','19894361','19898481','19898601','19900101','19900611','19900837','19902693','19902892','19897084')
----and upper(md.Email_Address) <>  upper(m.email_address) collate Latin1_General_CI_AI

---- phone
--SELECT	md.member_reference, md.Home_Phone as OldHomeValue,  m.phone_no as NewHomeValue
--, md.Mobile as OldMobileValue,  m.phone_mobile_no as NewMobileValue
--FROM [KS_DEV].[dbo].member_details md
--join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m on md.Member_Reference collate Latin1_General_CI_AI = m.plan_member_ref
----where md.Date_Last_Updated >= '28-Jun-2017'
--where m.plan_member_id in ('19892882','19893013','19892484','19892495','19892527','19893543','19895830','19893391','19893599','19896348','19894302','19897724','19894361','19898481','19898601','19900101','19900611','19900837','19902693','19902892','19897084')

---- names
--SELECT	md.member_reference, md.Mbr_Active_Status as OldActiveStatus,  m.account_Status as NewActiveStatus
--,md.Title as OldTitle, m.title_name as NewTitle, md.Given_Names as OldNames, m.first_name + ' ' + m.middle_name as NewNames,
--md.Surname as OldLastName, m.last_name as NewLastName
--FROM [KS_DEV].[dbo].member_details md
--join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m on md.Member_Reference collate Latin1_General_CI_AI = m.plan_member_ref
----where md.Date_Last_Updated >= '28-Jun-2017'
--where m.plan_member_id in ('19913950','19920989','19932184','19973694','19974380','19986845','20003200','19967673','19999618')

--SELECT	count(*)
--FROM [KS_DEV].[dbo].member_details md
--join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m on md.Member_Reference collate Latin1_General_CI_AI = m.plan_member_ref
----where md.Date_Last_Updated >= '28-Jun-2017'
--where m.account_status <> md.Mbr_Active_Status collate Latin1_General_CI_AI 
--and m.member_status = 'Open'

--select distinct Mbr_Active_Status from member_details
--select distinct account_status from [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m 

--select count(*) from [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m 
--where m.account_status = 'Closed'
--and m.member_status = 'Open'

---- tfn_status
--select m.[Plan_Member_ID]
--      ,mf.[TFN_Status] as OldTFNStatus
--      ,m.[TFN_Status] as NewTFNStatus
--FROM [KS_DEV].[dbo].member_flags mf
--join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m on mf.Member_Reference collate Latin1_General_CI_AI = m.plan_member_ref
--where m.plan_member_id in ('19947001','19989514','20026443','20189185','20269279','20348449','20351297','20380485','20531789','20856972','20875873','20966000','20970402','20975615','20978048','20983034','20991685','20996468','21004992','21013064')

-- web profile
select m.[Plan_Member_ID]
	,      coalesce(m.[Web_Profile],'No') as Web_Profile
      ,m.[Web_Profile] as NewWebProfile
      ,m.[Web_Profile_Create_Date] as NewWebCreateDate
      ,mf.[Web_Profile] as oldProfile
      ,mf.Date_Web_Created as oldCreateDate
FROM [KS_DEV].[dbo].member_flags mf
join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m on mf.Member_Reference collate Latin1_General_CI_AI = m.plan_member_ref
where m.plan_member_id in ('19967167','19990087','19993344','19997266','20040675','20058574','20072674','20085584','20090416','20120457','20121943','20122724','20129382','20139739','20151483','20189185','20203228','20210504','20211022')

-- rollins
select m.[Plan_Member_ID]
      ,m.[Last_Roll_In_Date]
      ,m.[Last_Roll_In_Amount]
	  ,mf.Date_Last_Rollin as OldRollinDate
	  ,mf.Last_Rollin_Amount as OldRollinAmt
	  ,r.effective_date
	  ,r.posted_date
	  ,r.rollover_amount
FROM [KS_DEV].[dbo].member_flags mf
join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m on mf.Member_Reference collate Latin1_General_CI_AI = m.plan_member_ref
join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].rollovers r on r.plan_member_id = m.plan_member_id
where m.plan_member_id in ('19963953','19993562','20058574','20072674','20073250','20081594','20085584','20090416','20096627','20104528','20120457','20131755','20139739','20145690','20185481','20193493','20201900','20210504')
and Last_Rollin_Amount <> [Last_Roll_In_Amount]

-- channel
select m.[Plan_Member_ID]
      ,mf.[mbr_joined_online] as Oldjoinchannel
      ,m.join_channel_name  as [newJoin_Channel_Name]
FROM [KS_DEV].[dbo].member_flags mf
join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m on mf.Member_Reference collate Latin1_General_CI_AI = m.plan_member_ref
where m.plan_member_id in ('19892754','19892732','19892766','19892840','19892865','19892961','19893020','19893280')


select m.[Plan_Member_ID]
     ,[Join_Channel_Name]
      --,coalesce([Marketing_Email_Opt_Out],'YES') as Marketing_Email_Opt_Out
      --,coalesce([Marketing_SMS_Opt_Out],'YES') as Marketing_SMS_Opt_Out
      --,coalesce([Marketing_DM_Opt_Out],'YES') as Marketing_DM_Opt_Out
      --,coalesce([Marketing_Phone_Opt_Out],'YES') as Marketing_Phone_Opt_Out
      --,[Web_Profile]
      --,[Web_Profile_Create_Date]
      --,[Web_Last_login_Date]
      --,[Welcome_Letter_Sent]
      ,mf.[TFN_Status] as OldTFNStatus
      ,m.[TFN_Status] as NewTFNStatus
     -- ,[SuperMatch_Consent]
     -- ,[SuperMatch_Consent_Nomination_Date]
     -- ,[Lost_Member]
     -- ,m.[Updatetime]
     -- ,[Default_Investment_Only]
     -- ,[Welcome_Letter_Print_Submitted_Date]
     --,convert(varchar(20),SUBSTRING(m.[Plan_Member_Ref], PATINDEX('%[^0]%',             m.[Plan_Member_Ref]+'.'), LEN(m.[Plan_Member_Ref]))) as BadMemberRef
     --,Privacy_Consent
     --,eStatement
     -- ,[Last_Roll_In_Date]
     -- ,[Last_Beneficiary_Nomination_Date]
     -- ,[Last_Roll_In_Amount]
FROM [KS_DEV].[dbo].member_flags mf
join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m on mf.Member_Reference collate Latin1_General_CI_AI = m.plan_member_ref
where m.plan_member_id in ('19963953','19993562','20058574','20072674','20073250','20081594','20085584','20090416','20096627','20104528','20120457','20131755','20139739','20145690','20185481','20193493','20201900','20210504')

select distinct mbr_joined_online from member_flags

select count(*)--top 100 member_reference, Join_Channel_Name
from member_flags mf
join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m on mf.Member_Reference collate Latin1_General_CI_AI = m.plan_member_ref
where Mbr_Joined_Online is null and join_channel_name <> 'Application'