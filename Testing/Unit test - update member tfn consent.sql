select mt.Member_Reference, mt.TFN_Valid, mta.TFN_Valid
from Member_TFN mt
join member_tfn_audit mta on mt.Member_Reference = mta.Member_Reference and mta.Audit_Date_Created >= dateadd(day, -1, current_timestamp)
where mt.Date_Last_Updated >= dateadd(day, -1, current_timestamp)