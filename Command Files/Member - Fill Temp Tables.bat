SET pkg="\\mefp01\groups\Data Management\Data Load\From Empirics\Packages\Member - Fill Temp Tables.dtsx"
SET log="\\mefp01\groups\Data Management\Data Load\From Empirics\Logs\Member_Fill_Temp_Tables.log"

echo Executing %pkg% starting at %date% %time% > %log%

REM UAT version
rem dtexec /rep e /f %pkg% /set \package.variables[User::DB_Name].Value;KS_DEV /set \package.variables[User::Run_Mode].Value;UPDATE >> %log% 2>&1

REM attempt at setting the logging providers
rem dtexec /rep e /f %pkg% /l "DTS.LogProviderTextFile;log.txt" /set \package.variables[User::DB_Name].Value;KS_DS /set \package.variables[User::Run_Mode].Value;UPDATE /set \package.variables[User::LogFileName].Value;%log%

REM PROD version
dtexec /rep e /f %pkg% /set \package.variables[User::DB_Name].Value;KS_DS /set \package.variables[User::Run_Mode].Value;UPDATE >> %log% 2>&1

echo Finished at %date% %time% >> %log%