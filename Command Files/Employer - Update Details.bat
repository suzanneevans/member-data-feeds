
SET pkg="\\mefp01\groups\Data Management\Data Load\From Empirics\Packages\Employer - Update Details.dtsx"
SET log="\\mefp01\groups\Data Management\Data Load\From Empirics\Logs\Employer_Update_Details.log"

echo Executing %pkg% starting at %date% %time% > %log%

REM UAT
rem dtexec /rep e /f %pkg%  /set \package.variables[User::DB_Name].Value;KS_DEV /set \package.variables[User::Run_Mode].Value;UPDATE >> %log% 2>&1

REM PROD
dtexec /rep e /f %pkg%  /set \package.variables[User::DB_Name].Value;KS_DS /set \package.variables[User::Run_Mode].Value;UPDATE >> %log% 2>&1



echo Finished at %date% %time% >> %log%