
SET pkg="\\mefp01\groups\Data Management\Data Load\From Empirics\Packages\Member - Update Member Details.dtsx"
SET log="\\mefp01\groups\Data Management\Data Load\From Empirics\Logs\Member_Update_Member_Details.log"

echo Executing %pkg% starting at %date% %time% > %log%

REM UAT
dtexec /rep e /f %pkg% /set \package.variables[User::DB_Name].Value;KS_DEV /set \package.variables[User::Run_Mode].Value;UPDATE

REM PROD
rem dtexec /rep e /f %pkg% /set \package.variables[User::DB_Name].Value;KS_DS /set \package.variables[User::Run_Mode].Value;UPDATE

echo Finished at %date% %time% >> %log%