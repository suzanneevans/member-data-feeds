
SET pkg="\\mefp01\groups\Data Management\Data Load\From Empirics\Packages\job test.dtsx"
SET log="\\mefp01\groups\Data Management\Data Load\From Empirics\Logs\job_test.log"

echo Executing %pkg% starting at %date% %time% > %log%

REM UAT
dtexec /rep e /f %pkg%  /set \package.variables[User::DB_Name].Value;KS_DEV /set \package.variables[User::test_value].Value;BAT_VAL /set \package.variables[User::Run_Mode].Value;UPDATE >> %log% 2>&1

REM PROD
rem dtexec /rep e /f %pkg%  /set \package.variables[User::DB_Name].Value;KS_DS /set \package.variables[User::Run_Mode].Value;UPDATE | tee.bat %log%



echo Finished at %date% %time% >> %log%