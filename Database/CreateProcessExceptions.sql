IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Process_Log'))
BEGIN
	PRINT 'ERROR: Run CreateProcessLog script first'
	RETURN
END
ELSE
BEGIN

	--drop table dbo.Process_Messages
	IF (NOT EXISTS (SELECT * 
					 FROM INFORMATION_SCHEMA.TABLES 
					 WHERE TABLE_SCHEMA = 'dbo' 
					 AND  TABLE_NAME = 'Process_Messages'))
	BEGIN

			CREATE TABLE dbo.Process_Messages (
				Message_Id integer IDENTITY,
				Run_Id integer,
				Table_Name varchar(100),
				Message_Type varchar(50),
				[Message] varchar(200)
				CONSTRAINT [PK_Process_Messages] PRIMARY KEY CLUSTERED 
				(
					Message_Id ASC
				)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]

			Print 'Process_Messages table created'
		END
		ELSE
			Print 'ERROR: Process_Messages table already exists'
END