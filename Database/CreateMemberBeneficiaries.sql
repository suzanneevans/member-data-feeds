-- drop table dbo.Member_Beneficiaries

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Member_Beneficiaries'))
BEGIN

	CREATE TABLE [dbo].[Member_Beneficiaries](
		[Member_Reference] [varchar](20) not NULL,
		[Effective_Date] [datetime] NULL,
		[Beneficiary_Name] [varchar](255) NULL,
		[Binding_Nomination_Start_Date] [datetime] NULL,
		[Binding_Nomination_End_Date] [datetime] NULL,
		[Relationship_Description] [varchar](8000) NULL,
		[Beneficiary_Percentage] [money] NULL,
		[Beneficiary_Type] [varchar](32) NULL,
		[Beneficiary_Status] [varchar](32) NULL,
		[Created_By] [varchar](50) NULL CONSTRAINT [Beneficiaries_LoadUser]  DEFAULT (suser_sname()),
		[Date_Created] [datetime] NULL CONSTRAINT [Beneficiaries_Loadtime]  DEFAULT (getdate()),
		[Last_Updated_By] [varchar](50) NULL CONSTRAINT [Beneficiaries_UpdateUser]  DEFAULT (suser_sname()),
		[Date_Last_Updated] [datetime] NULL CONSTRAINT [Beneficiaries_Updatetime]  DEFAULT (getdate())
	) ON [PRIMARY]

	Print 'Member_Beneficiaries table created'
END
ELSE
	Print 'ERROR: Member_Beneficiaries table already exists'
	
GO


