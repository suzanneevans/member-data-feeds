SELECT [Member_Account_Type]
      ,m.[Plan_Member_Ref]
      ,m.[Plan_Member_ID]

     ,[Join_Channel_Name]
      ,coalesce([Marketing_Email_Opt_Out],'YES') as Marketing_Email_Opt_Out
      ,coalesce([Marketing_SMS_Opt_Out],'YES') as Marketing_SMS_Opt_Out
      ,coalesce([Marketing_DM_Opt_Out],'YES') as Marketing_DM_Opt_Out
      ,coalesce([Marketing_Phone_Opt_Out],'YES') as Marketing_Phone_Opt_Out
      ,[Web_Profile]
      ,[Web_Profile_Create_Date]
      ,[Web_Last_login_Date]
      ,[Welcome_Letter_Sent]
      ,[TFN_Status]
      ,[SuperMatch_Consent]
      ,[SuperMatch_Consent_Nomination_Date]
      ,[Lost_Member]
      ,m.[Updatetime]
      ,[Default_Investment_Only]
      ,[Welcome_Letter_Print_Submitted_Date]
     ,convert(varchar(20),SUBSTRING(m.[Plan_Member_Ref], PATINDEX('%[^0]%',             m.[Plan_Member_Ref]+'.'), LEN(m.[Plan_Member_Ref]))) as BadMemberRef
     ,Privacy_Consent
     ,eStatement
      ,[Last_Roll_In_Date]
      ,[Last_Beneficiary_Nomination_Date]
      ,[Last_Roll_In_Amount]
from Member m
  join contact_details c on m.Plan_Member_ID = c.Plan_Member_ID and Contact_Type = 'Primary'
where  (member_account_type = 'KINE Emp Sponsored' or member_account_type = 'KINE Personal')
and (Member_Status = 'Open' or [Account_Closed_Date] >= '01-Apr-2017')