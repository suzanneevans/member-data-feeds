/****** Script for SelectTopNRows command from SSMS  ******/
SELECT -- no broker code, no consultant code (obsolete)
      Source_Member_Id as Member_Id
	  ,m.Plan_Member_Ref
      ,m.Title_Name
      ,m.Last_Name
      ,rtrim(concat(m.First_Name,' ',m.Middle_Name)) as GivenNames
      ,m.Joined_Fund_Date
	  -- no delivery source (need to have this)
      ,m.Birth_Date
	  ,Fund_ABN
	  ,Fund_Name_Source
	  ,Posted_Date
	  ,Effective_Date
	  ,Rollover_Type
	  ,Rollover_Amount
	  ,e.Plan_Employer_Ref
	  ,e.Employer_Trading_Name
	  ,e.Employer_Registered_Business_Name
	  ,rtrim(concat(cd.Source_Line_1_Address,' ',cd.Source_Line_2_Address)) as MemberAddress
	  ,cd.Source_Suburb_Name
	  ,cd.Source_State_Code
	  ,cd.Source_Postal_Code
	  ,cd.Source_Country_Name
	  ,m.Last_Contribution_Date
	  ,'Kinetic Super' as [Fund_Category]
      ,m.Member_Account_Type
	  ,m.Email_Address
	  ,m.Phone_Mobile_No
	  ,m.Phone_No
  FROM [Staging_AAS_Empirics_2016].[dbo].[Rollovers] ro
  join Member m on ro.Plan_Member_ID = m.Plan_Member_ID
  join Contact_Details cd on m.Plan_Member_ID = cd.Plan_Member_ID and cd.Contact_Type = 'Primary'
  join Employer e on m.Current_Employer_Id = e.Plan_Employer_ID
  where posted_date > '01-Jul-2017'

