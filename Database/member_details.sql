SELECT 
[Member_Account_Type]
      ,m.[Plan_Member_Ref]
      ,m.[Plan_Member_ID]
      ,[Joined_Fund_Date]
      ,m.[Gender]
--      ,m.[Title_Name]
--      ,m.[First_Name]
--      ,m.[Last_Name]
	  ,left(left(c.Source_Title,1) + lower(right(c.Source_Title, len(c.Source_Title)-1)),100) as Title_Name
	  ,left(c.Source_First_Name,50) as First_Name
	  ,left(c.Source_Last_Name,110) as Last_Name
      ,m.[Birth_Date]
      ,m.[Member_Age]
      ,case when m.[Email_Address] = 'null@null.com' then null else m.[Email_Address] end as Email_Address
      ,m.[Phone_Mobile_No]
      ,m.[Phone_No]
      ,[BPAY_Reference_Number]
      ,[Account_Status]
      ,[Member_Status]
      ,m.[Updatetime]
      ,[Account_Closed_Date]
     ,convert(varchar(20),SUBSTRING(m.[Plan_Member_Ref], PATINDEX('%[^0]%',             m.[Plan_Member_Ref]+'.'), LEN(m.[Plan_Member_Ref]))) as BadMemberRef
from Member m
  join contact_details c on m.Plan_Member_ID = c.Plan_Member_ID and Contact_Type = 'Primary'
where  (member_account_type = 'KINE Emp Sponsored' or member_account_type = 'KINE Personal')
and (Member_Status = 'Open' or [Account_Closed_Date] >= '01-Apr-2017')