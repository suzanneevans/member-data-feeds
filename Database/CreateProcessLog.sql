-- drop table dbo.Process_Log

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Process_Log'))
BEGIN

	CREATE TABLE dbo.Process_Log (
		Run_Id integer IDENTITY,
		Package_Name varchar(100),
		Package_Version varchar(10),
		[Start_Date] datetime,
		End_Date datetime,
		Record_Count integer,
		Error_Count integer,
		Run_Status varchar(25)
		CONSTRAINT [PK_Process_Log] PRIMARY KEY CLUSTERED 
		(
			Run_Id ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	Print 'Process_Log table created'
END
ELSE
	Print 'ERROR: Process_Log table already exists'
