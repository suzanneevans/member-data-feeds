-- Member Details - actual date of death from death certificate
ALTER TABLE Member_Details ADD Date_Of_Death date null
ALTER TABLE Member_Details_AUDIT ADD Date_Of_Death date null

-- Member Details - breakdown of given names into first and middle

ALTER TABLE Member_Details ADD First_Name varchar(50) null
ALTER TABLE Member_Details_AUDIT ADD First_Name varchar(50) null

ALTER TABLE Member_Details ADD Middle_Name varchar(50) null
ALTER TABLE Member_Details_AUDIT ADD Middle_Name varchar(50) null

-- Member Flags - preliminary notification that a death has occurred
ALTER TABLE Member_Flags ADD Death_Advised varchar(5) not null DEFAULT 'NO'
ALTER TABLE Member_Flags_AUDIT ADD Death_Advised varchar(5) null

-- Member Flags - extend TFN consent for SM2
ALTER TABLE Member_Flags ALTER COLUMN Lost_Super_Consent varchar(32)