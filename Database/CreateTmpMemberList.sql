-- drop table dbo.Process_Log

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'tmpMemberList'))
BEGIN

	CREATE TABLE dbo.tmpMemberList ([Member_Reference] [varchar](20) not NULL) 

END
ELSE
BEGIN
	truncate table tmpMemberList
END
